r-bioc-gosemsim (2.32.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 13 Jan 2025 14:54:25 +0100

r-bioc-gosemsim (2.32.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 29 Nov 2024 11:41:41 +0100

r-bioc-gosemsim (2.30.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Thu, 05 Sep 2024 09:38:35 +0200

r-bioc-gosemsim (2.30.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 17 Aug 2024 21:26:31 +0200

r-bioc-gosemsim (2.30.0-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 30 Jul 2024 16:34:13 +0200

r-bioc-gosemsim (2.30.0-1~0exp) experimental; urgency=medium

  * Team upload
  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 17:07:38 +0200

r-bioc-gosemsim (2.28.1-1) unstable; urgency=medium

  * New upstream version
  * Set upstream metadata fields: Archive.

 -- Andreas Tille <tille@debian.org>  Wed, 07 Feb 2024 08:26:31 +0100

r-bioc-gosemsim (2.28.0-1) unstable; urgency=medium

  * New upstream version
  * dh-update-R to update Build-Depends (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 30 Nov 2023 14:37:15 +0100

r-bioc-gosemsim (2.26.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)
  * Lintian-overrides (see lintian bug #1017966)

 -- Andreas Tille <tille@debian.org>  Wed, 26 Jul 2023 22:34:49 +0200

r-bioc-gosemsim (2.24.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Reduce piuparts noise in transitions (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 21 Nov 2022 16:26:49 +0100

r-bioc-gosemsim (2.20.0-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Thu, 25 Nov 2021 14:43:04 +0100

r-bioc-gosemsim (2.18.1-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)
  * Provide autopkgtest-pkg-r.conf to make sure testthat will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Mon, 06 Sep 2021 20:41:59 +0200

r-bioc-gosemsim (2.16.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Tue, 03 Nov 2020 10:25:38 +0100

r-bioc-gosemsim (2.14.2-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Tue, 08 Sep 2020 21:21:54 +0200

r-bioc-gosemsim (2.14.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Tue, 28 Jul 2020 09:27:29 +0200

r-bioc-gosemsim (2.14.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Wed, 20 May 2020 09:38:40 +0200

r-bioc-gosemsim (2.12.1-2) unstable; urgency=medium

  * Trim trailing whitespace.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

 -- Andreas Tille <tille@debian.org>  Sat, 04 Apr 2020 07:32:12 +0200

r-bioc-gosemsim (2.12.1-1) unstable; urgency=medium

  * Initial release (closes: #954867)

 -- Andreas Tille <tille@debian.org>  Tue, 24 Mar 2020 17:15:18 +0100
