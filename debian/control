Source: r-bioc-gosemsim
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-bioc-annotationdbi,
               r-cran-dbi,
               r-cran-digest,
               r-bioc-go.db,
               r-cran-rlang,
               r-cran-r.utils,
               r-cran-yulab.utils,
               r-cran-rcpp,
               architecture-is-64-bit
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-gosemsim
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-gosemsim.git
Homepage: https://bioconductor.org/packages/GOSemSim/
Rules-Requires-Root: no

Package: r-bioc-gosemsim
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GO-terms semantic similarity measures
 The semantic comparisons of Gene Ontology (GO) annotations provide
 quantitative ways to compute similarities between genes and gene groups,
 and have became important basis for many bioinformatics analysis approaches.
 GOSemSim is an R package for semantic similarity computation among GO terms,
 sets of GO terms, gene products and gene clusters. GOSemSim implemented five
 methods proposed by Resnik, Schlicker, Jiang, Lin and Wang respectively.
